abstract class ColoredCards extends Card{
    protected String color;

    public ColoredCards(String color){
        this.color = color;
    }
    public String getColor(){
        return this.color;
    }

    public boolean canPlay(Card card){
        if(card instanceof ColoredCards){
           String cardColor = ((ColoredCards) card).getColor();
           if(cardColor.equals(this.color)){
               return true;
           }
        }
        return false;
    }
}