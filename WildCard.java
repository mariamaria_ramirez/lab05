import java.util.Scanner;

public class WildCard extends Wild{

    Scanner input = new Scanner(System.in);
    protected String newColor = input.nextLine();

    public WildCard(String newColor) {
        super(newColor);
    }
}