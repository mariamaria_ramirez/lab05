public class Reverse extends Special{

    public Reverse(String color, boolean clockDirection) {
        super(color);
        this.clockDirection = clockDirection;
    }

    protected boolean clockDirection = true;

    public void changeDirection(){
        if (this.clockDirection){
            this.clockDirection = false;
        }

        else {
            this.clockDirection = true;
        }
    }
}
