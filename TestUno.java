import java.beans.Transient;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

public class TestUno {
    @Test
    //Test NumberedCards
    void testCanPlayNumberCards(){
        NumberedCards card1 = new NumberedCards(2, "red");
        boolean result = true;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }

    @Test
    void testCanPlayNumberCards2(){
        NumberedCards card1 = new NumberedCards(8, "green");
        boolean result = true;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }

    @Test
    void testCanPlayNumberCards3(){
        NumberedCards card1 = new NumberedCards(18, "yellow");
        boolean result = false;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }

    //Test Reverse
    @Test
    void testCanPlayReverse(){
        Reverse card1 = new Reverse(10, "yellow");
        boolean result = true;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }

    //Test Skip
    @Test
    void testCanPlaySKip(){
        Skip card1 = new Skip(11, "green");
        boolean result = false;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }

    //Test PickUp2
    @Test
    void testCanPlayPickUp2(){
        pickUp2 card1 = new pickUp2(12, "red");
        boolean result = false;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }

    //Test WildCard
    @Test
    void testCanPlayPickUp2(){
        pickUp2 card1 = new pickUp2(12, "red");
        boolean result = false;
        boolean methodResult = card1.canPlay(card1);
        assertEquals(result, methodResult);
    }
}