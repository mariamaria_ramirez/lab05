public class NumberedCards extends ColoredCards{
    protected int value;

    public NumberedCards(int value, String color){
        super(color);
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

    public boolean canPlay(Card card){
        if (card instanceof NumberedCards){
            int cardValue = ((NumberedCards) card).getValue();
            if (cardValue == this.value){
                return true;
            }
        }
        return false;
    }
}