public class Skip extends Special{

    protected boolean skip = true;

    public Skip(String color, boolean skip) {
        super(color);
        this.skip = skip;
    }
    
    public boolean canPlay(){
        return false;
    }
}