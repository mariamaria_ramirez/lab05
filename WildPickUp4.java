public class WildPickUp4 extends Wild{

    protected int drawAmount = 4;

    public WildPickUp4(String newColor, int drawAmount) {
        super(newColor);
        this.drawAmount = drawAmount;
    }
}