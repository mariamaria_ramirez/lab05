abstract class Wild extends Card{
    protected String newColor;

    public Wild(String newColor){
        this.newColor = newColor;
    }

    public String getNewColor(){
        return this.newColor;
    }

    public boolean canPLay(){
        return true;
    }
}
