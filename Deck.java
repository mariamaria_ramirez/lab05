import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Deck{
    Scanner input = new Scanner(System.in);
    protected String newColor = input.nextLine();
    protected int regularNum = 2;
    protected int specialNum = 2;
    protected int wildNum = 4;
    protected int shuffle = 1;
    protected Random rand;
    protected ArrayList<Card> cards = new ArrayList<Card>();

    public Deck(){
        rand = new Random();
        
        for (int i = 0 ; i <= 9 ; i ++){
            cards.add(new NumberedCards(i, "red"));
            cards.add(new NumberedCards(i, "red"));
            cards.add(new NumberedCards(i, "blue"));
            cards.add(new NumberedCards(i, "blue"));
            cards.add(new NumberedCards(i, "green"));
            cards.add(new NumberedCards(i, "green"));
            cards.add(new NumberedCards(i, "yellow"));
            cards.add(new NumberedCards(i, "yellow"));
        }
        
        for(int i = 0 ; i < specialNum ; i ++){
            cards.add(new Skip("red", true));
            cards.add(new Skip("yellow", true));
            cards.add(new Skip("green", true));
            cards.add(new Skip("blue", true));

            cards.add(new Reverse("red", false));
            cards.add(new Reverse("yellow", false));
            cards.add(new Reverse("green", false));
            cards.add(new Reverse("blue", false));

            cards.add(new pickUp2("red", 2));
            cards.add(new pickUp2("yellow", 2));
            cards.add(new pickUp2("green", 2));
            cards.add(new pickUp2("blue", 2));
        }

        for(int i = 0 ; i < wildNum ;i ++){
            cards.add(new WildCard(this.newColor));
            cards.add(new WildPickUp4(this.newColor, wildNum));
        }
    }

    public void addToDeck(Card card){
        cards.add(card);
    }

    public Card draw(){
        return cards.remove(0);
    }

    public void shuffle(){
        for (int i = 0 ; i < shuffle * cards.size() ; i++){
            int x = rand.nextInt(cards.size());
            int y = rand.nextInt(cards.size());
            Card temp = cards.get(x);
            cards.set(x,cards.get(y));
            cards.set(y,temp);
        }
    }
}
